var http = require('http');
var router = require('agraddy.http.router');

var server = http.createServer(router.handler).listen(80);
server.timeout = 0;
