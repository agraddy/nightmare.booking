var mod = {};

var luggage = require('agraddy.luggage');
var post = require('agraddy.luggage.parse.post');
var nm = require('../nightmare');

mod.get = {};
mod.post = {};

mod.get['/'] = function(req, res) {
	res.writeHead(406, {'Content-Type': 'application/json'});
	res.end(JSON.stringify({message: 'Need to make requests using POST.'}));
}

mod.post['/'] = function(req, res) {
	luggage(req, res, [
			post,
			checkData,
			getData
		], function(err, req, res, lug) {
			console.log('err');
			console.log(err);
			if(err) {
				res.writeHead(406, {'Content-Type': 'application/json'});
				res.end(JSON.stringify({message: err.toString()}));
			} else {
				res.writeHead(200, {'Content-Type': 'application/json'});
				res.end(JSON.stringify(lug.output, null, 2));
			}
	});
}

function checkData(req, res, lug, cb) {
	var err;
	var test;

	test = 'arrival_date';
	if(!/^\d\d\d\d\d\d\d\d$/.test(lug.post[test])) {
		err = new Error(test + ' is not formatted correctly.');
	}

	test = 'departure_date';
	if(!/^\d\d\d\d\d\d\d\d$/.test(lug.post[test])) {
		err = new Error(test + ' is not formatted correctly.');
	}

	test = 'length';
	if(!/^\d*$/.test(lug.post[test])) {
		err = new Error(test + ' is not formatted correctly.');
	}

	test = 'lengthunits';
	if(['feet', 'metres'].indexOf(lug.post[test]) == -1) {
		err = new Error(test + ' is not formatted correctly.');
	}

	test = 'weight';
	if(!/^\d*$/.test(lug.post[test])) {
		err = new Error(test + ' is not formatted correctly.');
	}

	test = 'weightunits';
	if(['tons', 'tonnes'].indexOf(lug.post[test]) == -1) {
		err = new Error(test + ' is not formatted correctly.');
	}

	test = 'draft';
	if(!/^\d*$/.test(lug.post[test])) {
		err = new Error(test + ' is not formatted correctly.');
	}

	test = 'draftunits';
	if(['feet', 'metres'].indexOf(lug.post[test]) == -1) {
		err = new Error(test + ' is not formatted correctly.');
	}

	test = 'beam';
	if(!/^\d*$/.test(lug.post[test])) {
		err = new Error(test + ' is not formatted correctly.');
	}

	test = 'beamunits';
	if(['feet', 'metres'].indexOf(lug.post[test]) == -1) {
		err = new Error(test + ' is not formatted correctly.');
	}

	cb(err);
}

function getData(req, res, lug, cb) {
	console.log('getData');
	nm.get(lug.post, function(err, locations) {
		console.log('err');
		console.log(err);
		console.log('nm response');
		console.log('locations');
		console.log(locations);
		lug.output = locations;
		cb(err);
	});
}

module.exports = mod;
