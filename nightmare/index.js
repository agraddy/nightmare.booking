var mod = {};

var Nightmare = require('nightmare');

var nightmare = new Nightmare();

var locations = [];
var items = [];
var error;

var data;

mod.get = function(post, cb) {
	error = '';
	nightmare = new Nightmare();
	data = post;
	data.departure_date = data.departure_date.slice(0, 2) + '-' + data.departure_date.slice(2, 4) + '-' + data.departure_date.slice(4);
	data.arrival_date = data.arrival_date.slice(0, 2) + '-' + data.arrival_date.slice(2, 4) + '-' + data.arrival_date.slice(4);

	data.lengthunits = data.lengthunits.charAt(0).toUpperCase() + data.lengthunits.slice(1);
	data.weightunits = data.weightunits.charAt(0).toUpperCase() + data.weightunits.slice(1);
	data.draftunits = data.draftunits.charAt(0).toUpperCase() + data.draftunits.slice(1);
	data.beamunits = data.beamunits.charAt(0).toUpperCase() + data.beamunits.slice(1);

	start(main(), null, function(err) {
			cb(err, items);
	});
}

function* main() {
	locations = yield nightmare                                                                                                                    
		.goto('https://bookings.rottnestislandonline.com/OnlineBooking.aspx')

		// Mooring only                                                                                                                        
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TripOverview_radVisitType_1')                                                
		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TripOverview_radVisitType_1')                                                
		.wait(waitForUnload())                                                                                                                   
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TripOverview_txtDepartureDate')                                 

		// Set date (do backwards so you can set both - doing arrival first updates departure)                                                 
		.insert('#dnn_ctr367_ReservationsWizard_reservationsWizard_TripOverview_txtDepartureDate', '')                                 
		.insert('#dnn_ctr367_ReservationsWizard_reservationsWizard_TripOverview_txtDepartureDate', data.departure_date)                                 
		.insert('#dnn_ctr367_ReservationsWizard_reservationsWizard_TripOverview_txtArrivalDate', '')                                   
		.insert('#dnn_ctr367_ReservationsWizard_reservationsWizard_TripOverview_txtArrivalDate', data.arrival_date)                                   
		.wait(3000)                                                                                                                   
		.wait('[name="dnn$ctr367$ReservationsWizard$reservationsWizard$StartNavigationTemplateContainerID$StartNextButton"]')                 

		// Click next                                                                                                                          
		.click('[name="dnn$ctr367$ReservationsWizard$reservationsWizard$StartNavigationTemplateContainerID$StartNextButton"]')                 
		.wait(waitForUnload())                                                                                                                   
		.wait(3000)                                                                                                                   
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_chkAdmissionFees')                                              

		// Acknowledge admission fees warning                                                                                                  
		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_chkAdmissionFees')                                              
		.wait(3000)                                          
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselRegistration')                                          

		// Input vessel details                                                                                                                
		.insert('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselRegistration', data.vesselregistration)                                
		.wait(3000)

		.insert('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselLength', data.length)
		.select('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlVesselLength', data.lengthunits)
		.wait(3000)

		.insert('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselWeight', data.weight)                                           
		.select('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlVesselWeight', data.weightunits)
		.wait(3000)

		.insert('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselDraft', data.draft)
		.select('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlVesselDraft', data.draftunits)
		.wait(3000)

		.insert('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselBeam', data.beam)
		.select('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlVesselBeam', data.beamunits)
		.wait(3000)

		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_rblIsVesselInsured_0')
		.wait(waitForUnload())
		.wait(3000)
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_rblAreVesselDetailsCorrect_0')                                   

		// Click correct (wait for js to load)
		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_rblAreVesselDetailsCorrect_0')                                  
		.wait(waitForUnload())                                                                                                                   
		.wait(3000)
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_rblSplitMooring_0')                                                   

		// Click split                                                                                                                         
		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_rblSplitMooring_0')                                             
		.wait(waitForUnload())                                                                                                                   
		.wait(3000)
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlLocations')                                                   

		// Get locations                                                                                                                          
		.evaluate(function() {                                                                                                              
				var locations = [];
				$('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlLocations').find('option').each(function() {
						locations.push($(this).val());
						});
				return locations.slice(1);
				});                             

	items = [];
	var exist;
	var temp;
	var subpage;
	for(var i = 0; i < locations.length; i++) {
		console.log('for: ' + i);
		exist = yield doesItExist(nightmare, locations[i]);

		console.log('exist: ' + exist);
		if(exist) {
			yield setupCheckmarks(nightmare);
			temp = yield getItems(nightmare)
				console.log(temp);


			// Unselect the checkboxes
			yield unselect(nightmare);
			items = items.concat(temp);

			subpage = yield subpageExists(nightmare);

			while(subpage) {
				yield nextPage(nightmare);

				console.log('subpage');

				yield setupCheckmarks(nightmare);
				temp = yield getItems(nightmare)
					console.log(temp);

				// Unselect the checkboxes
				yield unselect(nightmare);
				items = items.concat(temp);

				subpage = yield subpageExists(nightmare);
			}
		}

		console.log('items');
		console.log(items);
	}
	console.log('final');
	console.log(items);

	console.log('end');
	nightmare.end();
}

function start(routine, data, cb) {
	result = routine.next(data);
	if(!error && !result.done) {
		result.value.then(function(data) {
			start(routine, data, cb);
		}, function(err) {
console.log(err);
			error = new Error('There was a problem processing the form. Please try again or contact the developer.');
			console.log('end');
			nightmare.end();
			cb(error);
		});
	} else if(error) {
		console.log('call error');
		cb(error);
	} else {
		console.log('call cb');
		cb();
	}
}

function waitForUnload() {
return null;
}
/*
function waitForUnload() {
	return new Promise(function(resolve, reject) {
			window.onbeforeunload = function() {
			resolve();
			};
			});
}
*/

function doesItExist(nightmare, location) {
	return nightmare
		.select('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlLocations', location)                                        
		.wait(3000 * 3)
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlLocations')                                                   
		.evaluate(function() {                                                                                                              
				console.log('eval');
				if($('.WizardDataTable').length) {
				return true;
				} else {
				return false;
				}
				});                             
}

function setupCheckmarks(nightmare) {
	return nightmare
		.evaluate(function() {                                                                                                              
				$('.WizardDataTable input[type=checkbox]').each(function() {
						$(this).attr('checked', 'checked');
						});
				$('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_lstAvailableRentalMoorings_ctrl0_chkSelected').attr('checked', '');
				return;
				});                             
}
function getItems(nightmare) {
	return nightmare
		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_lstAvailableRentalMoorings_ctrl0_chkSelected')
		.wait(3000 * 3)
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_lstAvailableRentalMoorings_ctrl0_chkSelected')                                                   
		.evaluate(function() {                                                                                                              
				var output = [];
				if($('.WizardDataTable').length) {
				$('.WizardDataTable tbody tr.WizardDataTableRowTop').each(function() {
						var item = {};
						item.mooring = $(this).find('td:nth-child(2)').text().replace(/\r?\n/g, '').trim();
						item.location = $(this).find('td:nth-child(3)').text().replace(/\r?\n/g, '').trim();
						item.type = $(this).find('td:nth-child(4)').text().replace(/\r?\n/g, '').trim();
						item.cost = $(this).find('td:nth-child(7)').text().replace(/\r?\n/g, '').trim();

						output.push(item);
						});
				return output;
				} else {
				return output;
				}
				});

}

function unselect(nightmare) {
	return nightmare
		.evaluate(function() {                                                                                                              
				console.log('eval');
				if($('.WizardDataTable').length) {
				console.log('one');
				$('.WizardDataTable input[type=checkbox]').each(function() {
						$(this).attr('checked', '');
						});
				$('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_lstAvailableRentalMoorings_ctrl0_chkSelected').attr('checked', 'checked');
				return true;
				} else {
				console.log('two');
				return false;
				}
				})
	.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_lstAvailableRentalMoorings_ctrl0_chkSelected')
		.wait(3000 * 3)
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_lstAvailableRentalMoorings_ctrl0_chkSelected');

}

function subpageExists(nightmare) {
	return nightmare
		.evaluate(function() {                                                                                                              
				if($('.WizardDataTablePager a:last-of-type').attr('disabled') != 'disabled') {
				return true;
				} else {
				return false;
				}
				});
}

function nextPage(nightmare) {
	return nightmare
		.click('.WizardDataTablePager a:last-of-type')
		.wait(3000 * 3)
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlLocations');
}

module.exports = mod;
