var Nightmare = require('nightmare');

var nightmare = new Nightmare();

start(main());

function* main() {
	var title = yield nightmare
		.goto('https://bookings.rottnestislandonline.com/OnlineBooking.aspx')

		// Mooring only
		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TripOverview_radVisitType_1')
		.wait(waitForUnload)

		// Set date (do backwards so you can set both - doing arrival first updates departure)
		.type('#dnn_ctr367_ReservationsWizard_reservationsWizard_TripOverview_txtDepartureDate', '09-03-2018')
		.wait(waitForUnload)
		.type('#dnn_ctr367_ReservationsWizard_reservationsWizard_TripOverview_txtArrivalDate', '08-03-2018')
		.wait(waitForUnload)

		// Click next
		.click('[name="dnn$ctr367$ReservationsWizard$reservationsWizard$StartNavigationTemplateContainerID$StartNextButton"]')
		//.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_chkAdmissionFees')
		.wait(waitForUnload)

		// Acknowledge admission fees warning
		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_chkAdmissionFees')
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselRegistration')

		// Input vessel details
		.type('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselRegistration', '123456')
		.type('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselLength', '46')
		.select('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlVesselLength', 'Feet')
		.type('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselWeight', '1')
		.select('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlVesselWeight', 'Tons')
		.type('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselDraft', '7')
		.select('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlVesselDraft', 'Feet')
		.type('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_txtVesselBeam', '14')
		.select('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlVesselBeam', 'Feet')
		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_rblIsVesselInsured_0')
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_rblAreVesselDetailsCorrect_0')

		// Click correct
		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_rblAreVesselDetailsCorrect_0')
		.wait('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_ddlLocations')

		// Click split
		.click('#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_rblSplitMooring_0')


		// Get values

		.evaluate(function(sel) {
				return document.querySelector(sel).innerText;
				}, '#dnn_ctr367_ReservationsWizard_reservationsWizard_TravelMethod_pnlDropDownLists > h2')
	.end();

	console.log(title);
}

function start(routine, data) {
	result = routine.next(data);
	if(!result.done) {
		result.value.then(function(data) {
				start(routine, data);  // continue next iteration of routine normally
				});
	}
}


// From https://github.com/segmentio/nightmare/issues/854#issuecomment-256658926
var waitForUnload = function() {   
	return new Promise(function(resolve, reject) {
			window.onbeforeunload = function() {
			resolve();
			};
			});
};

//var title = await nightmare.goto('http://yahoo.com').title();
//await nightmare.end();


